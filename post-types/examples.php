<?php

/**
 * Registers the `examples` post type.
 */
function examples_init() {
	register_post_type( 'examples', array(
		'labels'                => array(
			'name'                  => __( 'Examples', 'zoo-types' ),
			'singular_name'         => __( 'Examples', 'zoo-types' ),
			'all_items'             => __( 'All Examples', 'zoo-types' ),
			'archives'              => __( 'Examples Archives', 'zoo-types' ),
			'attributes'            => __( 'Examples Attributes', 'zoo-types' ),
			'insert_into_item'      => __( 'Insert into Examples', 'zoo-types' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Examples', 'zoo-types' ),
			'featured_image'        => _x( 'Featured Image', 'examples', 'zoo-types' ),
			'set_featured_image'    => _x( 'Set featured image', 'examples', 'zoo-types' ),
			'remove_featured_image' => _x( 'Remove featured image', 'examples', 'zoo-types' ),
			'use_featured_image'    => _x( 'Use as featured image', 'examples', 'zoo-types' ),
			'filter_items_list'     => __( 'Filter Examples list', 'zoo-types' ),
			'items_list_navigation' => __( 'Examples list navigation', 'zoo-types' ),
			'items_list'            => __( 'Examples list', 'zoo-types' ),
			'new_item'              => __( 'New Examples', 'zoo-types' ),
			'add_new'               => __( 'Add New', 'zoo-types' ),
			'add_new_item'          => __( 'Add New Examples', 'zoo-types' ),
			'edit_item'             => __( 'Edit Examples', 'zoo-types' ),
			'view_item'             => __( 'View Examples', 'zoo-types' ),
			'view_items'            => __( 'View Examples', 'zoo-types' ),
			'search_items'          => __( 'Search Examples', 'zoo-types' ),
			'not_found'             => __( 'No Examples found', 'zoo-types' ),
			'not_found_in_trash'    => __( 'No Examples found in trash', 'zoo-types' ),
			'parent_item_colon'     => __( 'Parent Examples:', 'zoo-types' ),
			'menu_name'             => __( 'Examples', 'zoo-types' ),
		),
		'public'                => true,
		'hierarchical'          => false,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'examples',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
		'taxonomies'          => array( 'category', 'post_tag' ),

	) );

}
add_action( 'init', 'examples_init' );

/**
 * Sets the post updated messages for the `examples` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `examples` post type.
 */
function examples_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['examples'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Examples updated. <a target="_blank" href="%s">View Examples</a>', 'zoo-types' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'zoo-types' ),
		3  => __( 'Custom field deleted.', 'zoo-types' ),
		4  => __( 'Examples updated.', 'zoo-types' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Examples restored to revision from %s', 'zoo-types' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Examples published. <a href="%s">View Examples</a>', 'zoo-types' ), esc_url( $permalink ) ),
		7  => __( 'Examples saved.', 'zoo-types' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Examples submitted. <a target="_blank" href="%s">Preview Examples</a>', 'zoo-types' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Examples scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Examples</a>', 'zoo-types' ),
		date_i18n( __( 'M j, Y @ G:i', 'zoo-types' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Examples draft updated. <a target="_blank" href="%s">Preview Examples</a>', 'zoo-types' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'examples_updated_messages' );


function example_custom_meta() {
    add_meta_box( 
		'example_meta', 
		__( 'Meta Box Title', 'zoo-types' ), 
		'example_meta_callback', 
		'examples' 
	);
}
add_action( 'add_meta_boxes', 'example_custom_meta' );


/**
 * Outputs the content of the meta box
 */
function example_meta_callback( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'example_nonce' );
    $example_stored_meta = get_post_meta( $post->ID );
    ?>
 
    <p>
        <label for="meta-url" class="example-row-title"><?php _e( 'URL', 'zoo-types' )?></label>
        <input type="text" name="meta-url" id="meta-url" value="<?php if ( isset ( $example_stored_meta['meta-url'] ) ) echo $example_stored_meta['meta-url'][0]; ?>" />
    </p>
	<p>
		<label for="meta-external">  <?php _e( 'Link website', 'zoo-types' )?></label>
		<input type="checkbox" name="meta-external" id="meta-external" value="yes" <?php if ( isset ( $example_stored_meta['meta-external'] ) ) checked( $example_stored_meta['meta-external'][0], 'yes' ); ?> />
	</p>
    <?php
}

/**
 * Saves the custom meta input
 */
function example_meta_save( $post_id ) {
 
    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'example_nonce' ] ) && wp_verify_nonce( $_POST[ 'example_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';
 
    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }
 
    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'meta-url' ] ) ) {
        update_post_meta( $post_id, 'meta-url', sanitize_text_field( $_POST[ 'meta-url' ] ) );
    }
	if( isset( $_POST[ 'meta-external' ] ) ) {
		update_post_meta( $post_id, 'meta-external', 'yes' );
	} else {
		update_post_meta( $post_id, 'meta-external', '' );
	}
	 
}
add_action( 'save_post', 'example_meta_save' );