<?php

/**
 * Registers the `testimonial` post type.
 */
function testimonial_init() {
	register_post_type( 'testimonial', array(
		'labels'                => array(
			'name'                  => __( 'Testimonials', 'zoo-types' ),
			'singular_name'         => __( 'Testimonial', 'zoo-types' ),
			'all_items'             => __( 'All Testimonials', 'zoo-types' ),
			'archives'              => __( 'Testimonial Archives', 'zoo-types' ),
			'attributes'            => __( 'Testimonial Attributes', 'zoo-types' ),
			'insert_into_item'      => __( 'Insert into Testimonial', 'zoo-types' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Testimonial', 'zoo-types' ),
			'featured_image'        => _x( 'Featured Image', 'testimonial', 'zoo-types' ),
			'set_featured_image'    => _x( 'Set featured image', 'testimonial', 'zoo-types' ),
			'remove_featured_image' => _x( 'Remove featured image', 'testimonial', 'zoo-types' ),
			'use_featured_image'    => _x( 'Use as featured image', 'testimonial', 'zoo-types' ),
			'filter_items_list'     => __( 'Filter Testimonials list', 'zoo-types' ),
			'items_list_navigation' => __( 'Testimonials list navigation', 'zoo-types' ),
			'items_list'            => __( 'Testimonials list', 'zoo-types' ),
			'new_item'              => __( 'New Testimonial', 'zoo-types' ),
			'add_new'               => __( 'Add New', 'zoo-types' ),
			'add_new_item'          => __( 'Add New Testimonial', 'zoo-types' ),
			'edit_item'             => __( 'Edit Testimonial', 'zoo-types' ),
			'view_item'             => __( 'View Testimonial', 'zoo-types' ),
			'view_items'            => __( 'View Testimonials', 'zoo-types' ),
			'search_items'          => __( 'Search Testimonials', 'zoo-types' ),
			'not_found'             => __( 'No Testimonials found', 'zoo-types' ),
			'not_found_in_trash'    => __( 'No Testimonials found in trash', 'zoo-types' ),
			'parent_item_colon'     => __( 'Parent Testimonial:', 'zoo-types' ),
			'menu_name'             => __( 'Testimonials', 'zoo-types' ),
		),
		'public'                => true,
		'hierarchical'          => true,
		'show_ui'               => true,
		'show_in_nav_menus'     => true,
		'supports'              => array( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes' ),
		'has_archive'           => true,
		'rewrite'               => true,
		'query_var'             => true,
		'menu_position'         => null,
		'menu_icon'             => 'dashicons-admin-post',
		'show_in_rest'          => true,
		'rest_base'             => 'testimonial',
		'rest_controller_class' => 'WP_REST_Posts_Controller',
	) );

}
add_action( 'init', 'testimonial_init' );

/**
 * Sets the post updated messages for the `testimonial` post type.
 *
 * @param  array $messages Post updated messages.
 * @return array Messages for the `testimonial` post type.
 */
function testimonial_updated_messages( $messages ) {
	global $post;

	$permalink = get_permalink( $post );

	$messages['testimonial'] = array(
		0  => '', // Unused. Messages start at index 1.
		/* translators: %s: post permalink */
		1  => sprintf( __( 'Testimonial updated. <a target="_blank" href="%s">View Testimonial</a>', 'zoo-types' ), esc_url( $permalink ) ),
		2  => __( 'Custom field updated.', 'zoo-types' ),
		3  => __( 'Custom field deleted.', 'zoo-types' ),
		4  => __( 'Testimonial updated.', 'zoo-types' ),
		/* translators: %s: date and time of the revision */
		5  => isset( $_GET['revision'] ) ? sprintf( __( 'Testimonial restored to revision from %s', 'zoo-types' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
		/* translators: %s: post permalink */
		6  => sprintf( __( 'Testimonial published. <a href="%s">View Testimonial</a>', 'zoo-types' ), esc_url( $permalink ) ),
		7  => __( 'Testimonial saved.', 'zoo-types' ),
		/* translators: %s: post permalink */
		8  => sprintf( __( 'Testimonial submitted. <a target="_blank" href="%s">Preview Testimonial</a>', 'zoo-types' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
		/* translators: 1: Publish box date format, see https://secure.php.net/date 2: Post permalink */
		9  => sprintf( __( 'Testimonial scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Testimonial</a>', 'zoo-types' ),
		date_i18n( __( 'M j, Y @ G:i', 'zoo-types' ), strtotime( $post->post_date ) ), esc_url( $permalink ) ),
		/* translators: %s: post permalink */
		10 => sprintf( __( 'Testimonial draft updated. <a target="_blank" href="%s">Preview Testimonial</a>', 'zoo-types' ), esc_url( add_query_arg( 'preview', 'true', $permalink ) ) ),
	);

	return $messages;
}
add_filter( 'post_updated_messages', 'testimonial_updated_messages' );


