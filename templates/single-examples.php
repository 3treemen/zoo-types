<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package zoo-types
 */

get_header();
?>

	<main id="primary" class="site-main">

		<h1>Examples</h1>
	</main><!-- #main -->

<?php
get_sidebar();
get_footer();
