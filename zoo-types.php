<?php
/**
 * Plugin Name:     Zoo Types
 * Plugin URI:      Post Types for ZOO
 * Description:     Custom post types for ZOO website
 * Author:          Ronnie Stevens
 * Author URI:      ronniestevens.co
 * Text Domain:     zoo-types
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Zoo_Types
 */

// Your code starts here.

require_once('post-types/examples.php');
require_once('post-types/testimonial.php');

function add_zootypes_scripts() {
  wp_enqueue_style('css', plugins_url('zoo-types/style.css'), array(), '1.1', 'all');
  wp_enqueue_script('zootypes-script', plugins_url('zoo-types/js/zootypes.js'), array( 'jquery' ), 1.2, true);
}
add_action( 'wp_enqueue_scripts', 'add_zootypes_scripts' );

 /**
  * Includes a custom, single template as included in a plugin. If
  * the template is being viewed for a custom post type then use it;
  * otherwise, use the template that's provided by WordPress at runtime.
  *
  * @param  string $originalTemplate the path to the original template
  *
  * @return string the path to the original template or the custom template.
  */
  function acmeIncludeSingleTemplate($originalTemplate)
  {
    $singleTemplate = plugin_dir_path(
      \dirname(
        __DIR__
      )
    );
    $singleTemplate .= 'plugins/zoo-types/templates/single-examples.php';
    if ('examples' === get_post_type(get_the_ID())) {
     //    echo $singleTemplate;
        if (file_exists($singleTemplate)) {
            return $singleTemplate;
        }
    }
    
    return $originalTemplate;
  }
  add_action('single_template', 'acmeIncludeSingleTemplate');
 
  add_image_size( 'example', 540, 450 );

// Get all examples for homepage
function home_examples() {
    $args = array(
        'order' => 'DESC',
        'post_type' => 'examples',
    );

    $the_query = new WP_Query( $args );
    
    // The Loop
    if ( $the_query->have_posts() ) {
        echo '<div class="example_wrapper">';
        echo '<h2>Recente projecten</h2>';
        while ( $the_query->have_posts() ) {
            $the_query->the_post();
            $thumb_url = get_the_post_thumbnail_url($post = null, $size = 'example');
            $posttags = get_the_tags();
            $web_url = get_post_meta(get_the_ID(), 'meta-url', true);
            $link_external = get_post_meta(get_the_ID(), 'meta-external', true);
            $page_link = get_permalink();
            if($link_external == 'yes') {
              $url = $web_url;
            }
            else {
              $url = $page_link;
            }
            $post_categories = get_categories(
              array(
                'orderby' => 'name',
                'order' => 'ASC',
              )
            );

            echo '<div class="portfolio-item">';
            echo '<a href="'. $url .'">';
            if($thumb_url) {
              echo "<img src='" . $thumb_url . "' alt='Featured Image' class='responsive' />";
            }
            echo '<div class="caption">';
            echo '<div>';
            echo '<h3>' . get_the_title() . '</h3>';
            if($posttags) {
              echo '<span class="tags">';
              foreach($posttags as $tag) {
                echo '#'.$tag->name . ' ';
              }
              echo '</span>';
            }
            if($link_external == 'yes' ) {
              echo '<span class="exlink">Bezoek website <img src="wp-content/plugins/zoo-types/images/external-link-alt-solid.svg" alt="link" /></span>';
            }
            else {
              echo '<span class="inlink">Lees meer...</span>';
            }
            echo '</div>';
            echo '</div>';
            echo '</a>';
            echo '</div>';
        }
        echo '</div>';
    } else {
        // no posts found
    }
    /* Restore original Post Data */
    wp_reset_postdata();
}

function home_testimonials() {
  $args = array ( 
    'post_type' => 'testimonial'
  );

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) {
    echo '<h2>Klanten over Ronnie</h2>';
    $count = 0;
    while ( $query->have_posts() ) {
      $query->the_post();
      $count++;
      if($count % 2 == 0 ) {
        $align = 'right';
      }
      else {
        $align = 'left';
      }
      $thumb_url = get_the_post_thumbnail_url($post = null, $size = 'full');
      echo '<div class="testimonial '. $align .'">';
      if($thumb_url) {
        echo "<img src='" . $thumb_url . "' alt='Featured Image' class='responsive' />";
      }
      echo '<div>' . the_content() . '</div>';
      echo '</div>';
    }
  }
}
function simple_testimonials() {
  $args = array ( 
    'post_type' => 'testimonial',
    'orderby' => 'menu_order',
    'order' => 'ASC',
  );

  $query = new WP_Query( $args );

  if ( $query->have_posts() ) {
    echo '<div class="col-12 center"><h2>Testimonials</h2></div>';
    while ( $query->have_posts() ) {
      $query->the_post();
      echo "<div class='col-md-6 col-12'>";
      echo '<div class="testimonial">';
      echo the_content();
      echo '</div>';
      echo '</div>';
    }
  }
}

// show order in admin listing
$MY_POST_TYPE = "testimonial"; // just for a showcase

// the basic support (menu_order is included in the page-attributes)
add_post_type_support($MY_POST_TYPE, 'page-attributes');

// add a column to the post type's admin
// basically registers the column and sets it's title
add_filter('manage_' . $MY_POST_TYPE . '_posts_columns', function ($columns) {
  $columns['menu_order'] = "Order"; //column key => title
  return $columns;
});

// display the column value
add_action( 'manage_' . $MY_POST_TYPE . '_posts_custom_column', function ($column_name, $post_id){
  if ($column_name == 'menu_order') {
     echo get_post($post_id)->menu_order;
  }
}, 10, 2); // priority, number of args - MANDATORY HERE! 

// make it sortable
$menu_order_sortable_on_screen = 'edit-' . $MY_POST_TYPE; // screen name of LIST page of posts
add_filter('manage_' . $menu_order_sortable_on_screen . '_sortable_columns', function ($columns){
  // column key => Query variable
  // menu_order is in Query by default so we can just set it
  $columns['menu_order'] = 'menu_order';
  return $columns;
});